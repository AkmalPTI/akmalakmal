<html>

<head>
	<title> Home </title>
	<link rel="stylesheet" href="style.css">
</head>
<body>

	<div class="container">
		<div class="header">
			<h1 class="judul">Profil</h1>
			<ul>
				<li><a href="/home">Profil</a></li>
				<li><a href="/kontak">Kontak</a></li>
			</ul>
		</div>


		<div class="content">

			<img src="akmalsepeda.jpg" width="200" height="300" align="center">

			<h2>Profil Akmal Gibran</h2> 

			<p> Nama saya Akmal Gibran, lahir di Kebumen, Jawa Tengah, pada 1 Agustus 2001. Saya menetap di Bali semenjak tahun 2005. Saya sedang berkuliah di program studi Pendidikan Teknik Informatika Universitas Pendidikan Ganesha. Kelak, saya ingin menjadi seorang jurnalis (terutama jurnalis media daring). Saya berharap bisa mendirikan suatu website portal berita, dan menjadi redaktur utama di dalamnya. </p>

			<p> Saya ingin mendirikan suatu situs yang berfokus kepada ulasan budaya kontemporer di Indonesia. Karena menurut saya, Indonesia memiliki skena kesenian pop yang menarik</p>

			<p> Alasan kenapa saya ingin melakukan hal-hal tersebut adalah karena saya merasa bahwa Indonesia butuh lebih banyak situs yang memfokuskan diri untuk mengekspos kesenian populer. Indonesia memiliki skena kesenian populer yang tidak kalah menariknya dengan skena kesenian tradisionalnya yang memang sudah termahsyur.</p>
			
		</div>

		<div class="footer">
			<p class="copy"> Copyright 2021 oleh Akmal Gibran </p>
		</div>
		</div>
	</div>
	


</body>
</html>